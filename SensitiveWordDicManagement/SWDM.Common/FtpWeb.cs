﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SWDM.Common
{
    /// <summary>
    /// ftpweb上传
    /// </summary>
    public static class FtpWeb
    {
        /// <summary>
        /// ftp上传文件
        /// </summary>
        /// <param name="server">ftp地址</param>
        /// <param name="user">ftp用户</param>
        /// <param name="password">ftp用户密码</param>
        /// <param name="remoteFileName">上传后保存的文件名字</param>
        /// <param name="localPath">本地路径</param>
        public static string UploadFile(string server, string user, string password, string remoteFileName, string localPath)
        {
            string strResult;
            try
            {
                // Get the object used to communicate with the server.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(server + "/" + remoteFileName);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(user, password);

                // Copy the contents of the file to the request stream.

                StreamReader sourceStream = new StreamReader(localPath, Encoding.GetEncoding("GB2312"));
                byte[] fileContents = Encoding.GetEncoding("GB2312").GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                strResult = "上传成功，状态" + response.StatusDescription;

                response.Close();
            }
            catch (Exception ex)
            {
                strResult = "出现异常";
                SaveTextLogExtend.WriteException(ex);
            }
            return strResult;
        }

        /// <summary>
        /// ftp下载文件
        /// </summary>
        /// <param name="server">ftp地址</param>
        /// <param name="user">ftp用户</param>
        /// <param name="password">ftp用户密码</param>
        /// <param name="remoteFileName">上传后保存的文件名字</param>
        /// <param name="localPath">本地路径</param>
        public static bool DownLoadFile(string server, string user, string password, string remoteFileName, string localPath, out string msg)
        {
            bool isResult;
            try
            {
                // Get the object used to communicate with the server.
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(server + "/" + remoteFileName);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(user, password);

                FtpWebResponse response = (FtpWebResponse)request.GetResponse();

                Stream responseStream = response.GetResponseStream();
                FileStream fs = new FileStream(localPath, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs, Encoding.GetEncoding("GB2312"));
                responseStream.CopyTo(sw.BaseStream);
                sw.Flush();
                sw.Close();
                fs.Close();
                response.Close();
                msg = "下载成功" + response.StatusDescription;
                isResult = true;
            }
            catch (Exception ex)
            {
                msg = "出现异常";
                SaveTextLogExtend.WriteException(ex);
                isResult = false;
            }
            return isResult;
        }
    }
}
