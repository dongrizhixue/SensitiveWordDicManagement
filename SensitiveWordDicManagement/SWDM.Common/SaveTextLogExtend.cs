﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWDM.Common
{
    public static class SaveTextLogExtend
    {
        /// <summary>
        /// 输出异常到文本文件
        /// </summary>
        /// <param name="methodName">方法名</param>
        /// <param name="ex">异常</param>
        public static void WriteErrorLog(string methodName, Exception ex)
        {
            SaveTextLog.WriteErrorLog("{" + methodName + "}\r\n" + ex);
        }

        /// <summary>
        /// 输出异常到文本文件
        /// </summary>
        /// <param name="methodName">方法名</param>
        /// <param name="errorDescription">异常</param>
        public static void WriteErrorLog(string methodName, string errorDescription)
        {
            SaveTextLog.WriteErrorLog("{" + methodName + "}\r\n" + errorDescription);
        }

        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="methodName">方法名</param>
        /// <param name="message">日志消息</param>
        public static void WriteOperateLog(string methodName, string message)
        {
            SaveTextLog.WriteOperateLog("{" + methodName + "}\r\n" + message);
        }
        /// <summary>
        /// 写系统操作日志
        /// </summary>
        /// <param name="methodName">方法</param>
        /// <param name="message">消息</param>
        public static void WriteSystemLog(string methodName, string message)
        {
            SaveTextLog.WriteSystemLog("{" + methodName + "}\r\n" + message);
        }

        /// <summary>
        /// 记录异常
        /// </summary>
        /// <param name="ex"></param>
        public static void WriteException(Exception ex)
        {
            SaveTextLog.WriteException("{" + ex.TargetSite + "}\r\n" + ex);
        }
    }
}
