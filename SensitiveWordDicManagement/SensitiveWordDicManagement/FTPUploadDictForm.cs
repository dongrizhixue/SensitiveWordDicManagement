﻿using System;
using System.IO;
using System.Windows.Forms;

namespace SensitiveWordDicManagement
{
    public partial class FtpUploadDictForm : Form
    {
        public FtpUploadDictForm()
        {
            InitializeComponent();
        }

        public FtpUploadDictForm(string localPath)
        {
            InitializeComponent();
            if (string.IsNullOrEmpty(localPath))
            {
                MessageBox.Show("您没有选择敏感词字典文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                txtDictFilePath.Text = localPath;
            }
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "文本文件(*.txt)|*.txt";
            openFile.ShowDialog();
            if (string.IsNullOrEmpty(openFile.FileName))
            {
                MessageBox.Show("您没有选择敏感词字典文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                txtDictFilePath.Text = openFile.FileName;
            }
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDictFilePath.Text))
            {
                MessageBox.Show("请您选择敏感词文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (File.Exists(txtDictFilePath.Text))
                {
                    string strFtpServer = System.Configuration.ConfigurationManager.AppSettings["FtpServer"];
                    string strFtpUser = System.Configuration.ConfigurationManager.AppSettings["FtpUser"];
                    string strFtpPassword = System.Configuration.ConfigurationManager.AppSettings["FtpPassword"];
                    string strFtpDictFileName = System.Configuration.ConfigurationManager.AppSettings["FtpDictFileName"];
                    if (string.IsNullOrEmpty(strFtpServer) || string.IsNullOrEmpty(strFtpUser) || string.IsNullOrEmpty(strFtpPassword) || string.IsNullOrEmpty(strFtpDictFileName))
                    {
                        MessageBox.Show("系统未配置敏感词字典上传路径", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        string strResult = SWDM.Common.FtpWeb.UploadFile(strFtpServer, strFtpUser, strFtpPassword, strFtpDictFileName, txtDictFilePath.Text);
                        MessageBox.Show(strResult, Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("请您选择敏感词文件不存在，请重新选择", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}

