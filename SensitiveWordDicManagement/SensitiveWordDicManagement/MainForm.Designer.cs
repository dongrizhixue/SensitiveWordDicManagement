﻿namespace SensitiveWordDicManagement
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.管理敏感词字典文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.导入敏感词字典ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.上传词典ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.AllowMerge = false;
            this.menuStripMain.BackColor = System.Drawing.SystemColors.Control;
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.管理敏感词字典文件ToolStripMenuItem,
            this.导入敏感词字典ToolStripMenuItem,
            this.上传词典ToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(784, 25);
            this.menuStripMain.TabIndex = 0;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // 管理敏感词字典文件ToolStripMenuItem
            // 
            this.管理敏感词字典文件ToolStripMenuItem.BackColor = System.Drawing.SystemColors.Control;
            this.管理敏感词字典文件ToolStripMenuItem.Name = "管理敏感词字典文件ToolStripMenuItem";
            this.管理敏感词字典文件ToolStripMenuItem.Size = new System.Drawing.Size(128, 21);
            this.管理敏感词字典文件ToolStripMenuItem.Text = "管理敏感词字典文件";
            this.管理敏感词字典文件ToolStripMenuItem.Click += new System.EventHandler(this.管理敏感词字典文件ToolStripMenuItem_Click);
            // 
            // 导入敏感词字典ToolStripMenuItem
            // 
            this.导入敏感词字典ToolStripMenuItem.Name = "导入敏感词字典ToolStripMenuItem";
            this.导入敏感词字典ToolStripMenuItem.Size = new System.Drawing.Size(104, 21);
            this.导入敏感词字典ToolStripMenuItem.Text = "导入敏感词字典";
            this.导入敏感词字典ToolStripMenuItem.Click += new System.EventHandler(this.导入敏感词字典ToolStripMenuItem_Click);
            // 
            // 上传词典ToolStripMenuItem
            // 
            this.上传词典ToolStripMenuItem.Name = "上传词典ToolStripMenuItem";
            this.上传词典ToolStripMenuItem.Size = new System.Drawing.Size(68, 21);
            this.上传词典ToolStripMenuItem.Text = "上传词典";
            this.上传词典ToolStripMenuItem.Click += new System.EventHandler(this.上传词典ToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.menuStripMain);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "MainForm";
            this.Text = "敏感词字典管理系统";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem 导入敏感词字典ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 管理敏感词字典文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 上传词典ToolStripMenuItem;
    }
}

