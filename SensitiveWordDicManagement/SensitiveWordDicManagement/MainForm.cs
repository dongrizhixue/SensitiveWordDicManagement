﻿using System;
using System.Windows.Forms;

namespace SensitiveWordDicManagement
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }


        private void 导入敏感词字典ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CheckChildFrmExist("导入敏感词字典"))
            {
                //激活窗体，但不处理
            }
            else
            {
                ImportSensitiveWordDictForm form = new ImportSensitiveWordDictForm();
                form.MdiParent = this;
                form.Show();
            }
        }

        private void 管理敏感词字典文件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (CheckChildFrmExist("管理敏感词字典文件"))
            {
                //返回
            }
            else
            {
                SensitiveWordDictFileManagementForm form = new SensitiveWordDictFileManagementForm();
                form.MdiParent = this;
                form.Show();
            }
        }
        public bool CheckChildFrmExist(string childFrmName)
        {
            foreach (Form childFrm in MdiChildren)
            {
                if (childFrm.Text == childFrmName)
                {
                    if (childFrm.WindowState == FormWindowState.Minimized)
                    {
                        childFrm.WindowState = FormWindowState.Normal;

                    }
                    childFrm.Activate();

                    return true;

                }
            }
            return false;
        }

        private void 上传词典ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FtpUploadDictForm form = new FtpUploadDictForm();
            form.MdiParent = this;
            form.Show();
        }
    }
}
