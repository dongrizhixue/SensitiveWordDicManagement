﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensitiveWordDicManagement
{
    public partial class AddSensitiveWordDicForm : Form
    {
        public AddSensitiveWordDicForm()
        {
            InitializeComponent();
        }
        public AddSensitiveWordDicForm(string filePath)
        {
            InitializeComponent();
            txtFilePath.Text = filePath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFilePath.Text))
            {
                MessageBox.Show("您未选择文件，请回到上一级窗体加载文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (File.Exists(txtFilePath.Text))
                {
                    if (string.IsNullOrEmpty(txtWord.Text))
                    {
                        MessageBox.Show("您未填写词条，请填写词条", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        List<string> listWordDict = SWDM.Manage.TextFileManage.GetListStringByFile(txtFilePath.Text);
                        var varlistWordDict = from n in listWordDict
                                              where n == txtWord.Text
                                              select n;
                        if (varlistWordDict.Any())
                        {
                            lblMsg.Text = "警告：您添加的词条已经存在。";
                        }
                        else
                        {
                            try
                            {
                                using (StreamWriter file = new StreamWriter(txtFilePath.Text, true, Encoding.Default))
                                {
                                    file.WriteLine(txtWord.Text);
                                    lblMsg.Text = "提示：您已经成功添加词条“" + txtWord.Text + "”";
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("您未选择的文件不存在，请回到上一级窗体加载文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txtWord_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
    }
}
