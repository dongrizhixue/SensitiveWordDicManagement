﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensitiveWordDicManagement
{
    public partial class ImportSensitiveWordDictForm : Form
    {
        public ImportSensitiveWordDictForm()
        {
            InitializeComponent();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            try
            {
                //判断源文件路径是否正确
                if (string.IsNullOrEmpty(txtSourceFilePath.Text))
                {
                    MessageBox.Show("请选择源文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (File.Exists(txtSourceFilePath.Text))
                    {
                        //判断目标文件路径是否正确
                        if (string.IsNullOrEmpty(txtPathOfTargetFile.Text))
                        {
                            MessageBox.Show("请选择目标文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            if (File.Exists(txtPathOfTargetFile.Text))
                            {
                                string msg;
                                //开始采集目标文件和源文件的数据
                                List<string> listStrSourceFile = GetListSout(out msg);
                                if (listStrSourceFile == null || listStrSourceFile.Count == 0)
                                {
                                    MessageBox.Show(msg, Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                }
                                else
                                {
                                    if (listStrSourceFile.Count == 1)
                                    {
                                        if (MessageBox.Show("导入文件只有一个词条，请检查分隔符是否正确，并区分大小写\r\n继续请按”是“，返回请按”否“", Text, MessageBoxButtons.YesNo) == DialogResult.No)
                                        {
                                            return;
                                        }
                                    }
                                    List<string> listStrTargetFile = SWDM.Manage.TextFileManage.GetListStringByFile(txtPathOfTargetFile.Text);
                                    if (listStrTargetFile == null || listStrTargetFile.Count == 0)
                                    {
                                        MessageBox.Show("目标文件没有数据", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    }
                                    else
                                    {
                                        List<string> listStreamWriter = new List<string>();
                                        foreach (string item in listStrSourceFile)
                                        {
                                            var varItem = from n in listStrTargetFile
                                                          where n == item
                                                          select n;
                                            if (!varItem.Any())
                                            {
                                                listStreamWriter.Add(item);
                                            }
                                        }
                                        if (listStreamWriter == null || listStreamWriter.Count == 0)
                                        {
                                            MessageBox.Show("源文件中的词条都已经在目标文件中存在，不需要导入了", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        }
                                        else
                                        {
                                            string path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + @"\DictBackup";
                                            if (Directory.Exists(path))
                                            {
                                                //不处理
                                            }
                                            else
                                            {
                                                Directory.CreateDirectory(path);
                                            }
                                            string destPath = Path.Combine(path, Path.GetFileNameWithoutExtension(txtPathOfTargetFile.Text) + DateTime.Now.ToString("yyyy年MM月dd日HH时mm分ss秒") + Path.GetExtension(txtPathOfTargetFile.Text));
                                            File.Copy(txtPathOfTargetFile.Text, destPath);
                                            using (StreamWriter file = new StreamWriter(txtPathOfTargetFile.Text, true, Encoding.Default))
                                            {
                                                foreach (string line in listStreamWriter)
                                                {
                                                    file.WriteLine(line);
                                                }
                                                MessageBox.Show("已经成功导入" + listStreamWriter.Count + "个词条", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {
                                MessageBox.Show("目标文件不存在", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("文件不存在。");
                    }
                }
            }
            catch (Exception ex)
            {
                SWDM.Common.SaveTextLogExtend.WriteException(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSelectSourceFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "文本文件(*.txt)|*.txt";
            openFile.ShowDialog();
            if (string.IsNullOrEmpty(openFile.FileName))
            {
                MessageBox.Show("没有选择文件。");
            }
            else
            {
                txtSourceFilePath.Text = openFile.FileName;
            }
        }

        private void btnOpenSourceFile_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSourceFilePath.Text))
            {
                MessageBox.Show("请选选择源文件");
            }
            else
            {
                if (File.Exists(txtSourceFilePath.Text))
                {
                    System.Diagnostics.Process.Start(txtSourceFilePath.Text);
                }
                else
                {
                    MessageBox.Show("文件不存在");
                }
            }
        }

        private void btnOpenSourceFolder_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSourceFilePath.Text))
            {
                MessageBox.Show("请选择源文件");
            }
            else
            {
                if (File.Exists(txtSourceFilePath.Text))
                {
                    System.Diagnostics.Process.Start("explorer.exe", Path.GetDirectoryName(txtSourceFilePath.Text));
                }
                else
                {
                    MessageBox.Show("文件不存在");
                }
            }
        }

        private void btnSelectTargetFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "文本文件(*.txt)|*.txt";
            open.ShowDialog();
            if (string.IsNullOrEmpty(open.FileName))
            {
                MessageBox.Show("您没有选择文件");
            }
            else
            {
                txtPathOfTargetFile.Text = open.FileName;
            }
        }

        private void btnOpenTargetFile_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPathOfTargetFile.Text))
            {
                MessageBox.Show("请选选择源文件");
            }
            else
            {
                if (File.Exists(txtPathOfTargetFile.Text))
                {
                    System.Diagnostics.Process.Start(txtPathOfTargetFile.Text);
                }
                else
                {
                    MessageBox.Show("文件不存在");
                }
            }
        }

        private void btnOpenTargetFolder_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPathOfTargetFile.Text))
            {
                MessageBox.Show("请选择源文件");
            }
            else
            {
                if (File.Exists(txtPathOfTargetFile.Text))
                {
                    System.Diagnostics.Process.Start("explorer.exe", Path.GetDirectoryName(txtPathOfTargetFile.Text));
                }
                else
                {
                    MessageBox.Show("文件不存在");
                }
            }
        }

        /// <summary>
        /// 私有方法，请确保文件存在
        /// </summary>
        /// <param name="msg">执行消息</param>
        /// <returns></returns>
        private List<string> GetListSout(out string msg)
        {
            List<string> listResult;
            if (rbtnEnter.Checked)
            {
                listResult = SWDM.Manage.TextFileManage.GetListStringByFile(txtSourceFilePath.Text);
                if (listResult == null || listResult.Count == 0)
                {
                    msg = "没有在源文件中获取到词条";
                }
                else
                {
                    msg = "成功";
                }
            }
            else if (radiobtnStr.Checked)
            {
                if (string.IsNullOrEmpty(textBox1.Text))
                {
                    msg = "请输入一个换行符";
                    listResult = null;
                }
                else
                {
                    string[] liststr = File.ReadAllText(txtSourceFilePath.Text, Encoding.Default).Split(textBox1.Text[0]);
                    if (liststr.Any())
                    {
                        msg = "";
                        listResult = liststr.ToList();
                    }
                    else
                    {
                        msg = "不存在此符号分隔词条";
                        listResult = null;
                    }
                }
            }
            else
            {
                msg = "请选择导入设置区域中的换行符";
                listResult = null;
            }
            return listResult;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "DictBackup");
            if (Directory.Exists(path))
            {
                System.Diagnostics.Process.Start("explorer.exe", path);
            }
            else
            {
                Directory.CreateDirectory(path);
            }
        }

        private void btnUploadDict_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPathOfTargetFile.Text))
            {
                MessageBox.Show("您还未加载文件，请点击“选择目标文件”按钮加载文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (File.Exists(txtPathOfTargetFile.Text))
                {
                    FtpUploadDictForm form = new FtpUploadDictForm(txtPathOfTargetFile.Text);
                    form.Show();
                }
                else
                {
                    MessageBox.Show("您选择的文件不存在，请重新选择", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnDownFile_Click(object sender, EventArgs e)
        {
            string strDownFileMsg;
            string strDownFilePath = SWDM.Manage.TextFileManage.DownLoadDictFile(out strDownFileMsg);

            if (string.IsNullOrEmpty(strDownFilePath))
            {
                if (string.IsNullOrEmpty(strDownFileMsg))
                {
                    MessageBox.Show("下载失败", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show(strDownFileMsg, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                txtPathOfTargetFile.Text = strDownFilePath;
            }
        }
    }
}
