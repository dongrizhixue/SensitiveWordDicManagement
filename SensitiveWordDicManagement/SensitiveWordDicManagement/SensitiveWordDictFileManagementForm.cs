﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensitiveWordDicManagement
{
    public partial class SensitiveWordDictFileManagementForm : Form
    {
        public SensitiveWordDictFileManagementForm()
        {
            InitializeComponent();
        }

        private void 查看文件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog opFile = new OpenFileDialog();
            opFile.Filter = "文本文件(*.txt)|*.txt";
            opFile.ShowDialog();
            if (string.IsNullOrEmpty(opFile.FileName))
            {
                MessageBox.Show("没有选择任何文件。", "提示", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                txtFullFileName.Text = opFile.FileName;
                LoadDataToDataGridView(opFile.FileName);
            }
        }

        private void LoadDataToDataGridView(string fileName)
        {
            List<string> listSensitiveWord = SWDM.Manage.TextFileManage.GetListStringByFile(fileName);
            if (listSensitiveWord == null)
            {
                MessageBox.Show("该文件没有数据");
            }
            else
            {
                dataGridView1.AutoGenerateColumns = false;
                toolStripStatusLabelWordCount.Text = listSensitiveWord.Count.ToString();
                dataGridView1.DataSource = (from n in listSensitiveWord
                                            orderby n
                                            select new { n }).ToList();
            }
        }

        private void 添加词条ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFullFileName.Text))
            {
                MessageBox.Show("请点击“载入和查看文件”按钮加载文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (File.Exists(txtFullFileName.Text))
                {
                    AddSensitiveWordDicForm form = new AddSensitiveWordDicForm(txtFullFileName.Text);
                    form.MdiParent = ParentForm;
                    form.Show();
                }
                else
                {
                    MessageBox.Show("您选择的文件不存在，请重新选择", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFullFileName.Text))
            {
                MessageBox.Show("您还未加载文件，请点击“载入和查看文件”按钮加载文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (File.Exists(txtFullFileName.Text))
                {
                    LoadDataToDataGridView(txtFullFileName.Text);
                }
                else
                {
                    MessageBox.Show("您选择的文件不存在，请重新选择", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void SensitiveWordDictFileManagementForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                toolStripMenuItem1_Click(sender, e);
            }
        }

        private void 上传ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFullFileName.Text))
            {
                MessageBox.Show("您还未加载文件，请点击“载入和查看文件”按钮加载文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (File.Exists(txtFullFileName.Text))
                {
                    FtpUploadDictForm form = new FtpUploadDictForm(txtFullFileName.Text);
                    form.Show();
                }
                else
                {
                    MessageBox.Show("您选择的文件不存在，请重新选择", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnSeacher_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFullFileName.Text))
            {
                MessageBox.Show("请点击“载入和查看文件”按钮加载文件", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (File.Exists(txtFullFileName.Text))
                {
                    List<string> listSensitiveWord = SWDM.Manage.TextFileManage.GetListStringByFile(txtFullFileName.Text);
                    if (listSensitiveWord == null)
                    {
                        MessageBox.Show("该文件没有数据");
                    }
                    else
                    {
                        dataGridView1.AutoGenerateColumns = false;

                        var varlistWord = from n in listSensitiveWord
                                          where n.IndexOf(txtKeyWord.Text) >= 0
                                          orderby n
                                          select new { n };
                        if (varlistWord.Any())
                        {
                            dataGridView1.DataSource = varlistWord.ToList();
                            toolStripStatusLabelWordCount.Text = varlistWord.Count().ToString();
                        }
                        else
                        {
                            dataGridView1.DataSource = null;
                            toolStripStatusLabelWordCount.Text = "0";
                        }
                    }
                }
                else
                {
                    MessageBox.Show("您选择的文件不存在，请重新选择", Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void 下载ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string strDownLoadMsg;
            string filePaht = SWDM.Manage.TextFileManage.DownLoadDictFile(out strDownLoadMsg);
            txtFullFileName.Text = filePaht;
            LoadDataToDataGridView(filePaht);
        }
    }
}
