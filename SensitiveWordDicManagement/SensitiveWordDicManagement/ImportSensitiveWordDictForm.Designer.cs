﻿namespace SensitiveWordDicManagement
{
    partial class ImportSensitiveWordDictForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnUploadDict = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.radiobtnStr = new System.Windows.Forms.RadioButton();
            this.rbtnEnter = new System.Windows.Forms.RadioButton();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSourceFilePath = new System.Windows.Forms.TextBox();
            this.btnOpenSourceFolder = new System.Windows.Forms.Button();
            this.btnOpenSourceFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectSourceFile = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtPathOfTargetFile = new System.Windows.Forms.TextBox();
            this.btnOpenTargetFolder = new System.Windows.Forms.Button();
            this.btnOpenTargetFile = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSelectTargetFile = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnDownFile = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnUploadDict);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.radiobtnStr);
            this.groupBox1.Controls.Add(this.rbtnEnter);
            this.groupBox1.Location = new System.Drawing.Point(12, 318);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(560, 105);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "导入设置";
            // 
            // btnUploadDict
            // 
            this.btnUploadDict.AutoSize = true;
            this.btnUploadDict.Location = new System.Drawing.Point(419, 76);
            this.btnUploadDict.Name = "btnUploadDict";
            this.btnUploadDict.Size = new System.Drawing.Size(135, 23);
            this.btnUploadDict.TabIndex = 7;
            this.btnUploadDict.Text = "上传目标文件到服务器";
            this.btnUploadDict.UseVisualStyleBackColor = true;
            this.btnUploadDict.Click += new System.EventHandler(this.btnUploadDict_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "请选择源文件的分割符号";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(77, 68);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 21);
            this.textBox1.TabIndex = 2;
            // 
            // radiobtnStr
            // 
            this.radiobtnStr.AutoSize = true;
            this.radiobtnStr.Location = new System.Drawing.Point(6, 70);
            this.radiobtnStr.Name = "radiobtnStr";
            this.radiobtnStr.Size = new System.Drawing.Size(71, 16);
            this.radiobtnStr.TabIndex = 1;
            this.radiobtnStr.TabStop = true;
            this.radiobtnStr.Text = "换行符：";
            this.radiobtnStr.UseVisualStyleBackColor = true;
            // 
            // rbtnEnter
            // 
            this.rbtnEnter.AutoSize = true;
            this.rbtnEnter.Location = new System.Drawing.Point(6, 48);
            this.rbtnEnter.Name = "rbtnEnter";
            this.rbtnEnter.Size = new System.Drawing.Size(47, 16);
            this.rbtnEnter.TabIndex = 0;
            this.rbtnEnter.TabStop = true;
            this.rbtnEnter.Text = "回车";
            this.rbtnEnter.UseVisualStyleBackColor = true;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(416, 429);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(75, 23);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "导入";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(497, 429);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "关闭";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtSourceFilePath);
            this.groupBox2.Controls.Add(this.btnOpenSourceFolder);
            this.groupBox2.Controls.Add(this.btnOpenSourceFile);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.btnSelectSourceFile);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(560, 147);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "源文件";
            // 
            // txtSourceFilePath
            // 
            this.txtSourceFilePath.Location = new System.Drawing.Point(6, 89);
            this.txtSourceFilePath.Multiline = true;
            this.txtSourceFilePath.Name = "txtSourceFilePath";
            this.txtSourceFilePath.ReadOnly = true;
            this.txtSourceFilePath.Size = new System.Drawing.Size(548, 21);
            this.txtSourceFilePath.TabIndex = 7;
            // 
            // btnOpenSourceFolder
            // 
            this.btnOpenSourceFolder.Location = new System.Drawing.Point(475, 116);
            this.btnOpenSourceFolder.Name = "btnOpenSourceFolder";
            this.btnOpenSourceFolder.Size = new System.Drawing.Size(75, 23);
            this.btnOpenSourceFolder.TabIndex = 6;
            this.btnOpenSourceFolder.Text = "打开文件夹";
            this.btnOpenSourceFolder.UseVisualStyleBackColor = true;
            this.btnOpenSourceFolder.Click += new System.EventHandler(this.btnOpenSourceFolder_Click);
            // 
            // btnOpenSourceFile
            // 
            this.btnOpenSourceFile.Location = new System.Drawing.Point(394, 116);
            this.btnOpenSourceFile.Name = "btnOpenSourceFile";
            this.btnOpenSourceFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenSourceFile.TabIndex = 5;
            this.btnOpenSourceFile.Text = "打开源文件";
            this.btnOpenSourceFile.UseVisualStyleBackColor = true;
            this.btnOpenSourceFile.Click += new System.EventHandler(this.btnOpenSourceFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "文件路径：";
            // 
            // btnSelectSourceFile
            // 
            this.btnSelectSourceFile.Location = new System.Drawing.Point(6, 29);
            this.btnSelectSourceFile.Name = "btnSelectSourceFile";
            this.btnSelectSourceFile.Size = new System.Drawing.Size(75, 23);
            this.btnSelectSourceFile.TabIndex = 0;
            this.btnSelectSourceFile.Text = "选择源文件";
            this.btnSelectSourceFile.UseVisualStyleBackColor = true;
            this.btnSelectSourceFile.Click += new System.EventHandler(this.btnSelectSourceFile_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnDownFile);
            this.groupBox3.Controls.Add(this.txtPathOfTargetFile);
            this.groupBox3.Controls.Add(this.btnOpenTargetFolder);
            this.groupBox3.Controls.Add(this.btnOpenTargetFile);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.btnSelectTargetFile);
            this.groupBox3.Location = new System.Drawing.Point(12, 165);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(560, 147);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "目标文件";
            // 
            // txtPathOfTargetFile
            // 
            this.txtPathOfTargetFile.Location = new System.Drawing.Point(6, 89);
            this.txtPathOfTargetFile.Multiline = true;
            this.txtPathOfTargetFile.Name = "txtPathOfTargetFile";
            this.txtPathOfTargetFile.ReadOnly = true;
            this.txtPathOfTargetFile.Size = new System.Drawing.Size(548, 21);
            this.txtPathOfTargetFile.TabIndex = 8;
            // 
            // btnOpenTargetFolder
            // 
            this.btnOpenTargetFolder.Location = new System.Drawing.Point(475, 116);
            this.btnOpenTargetFolder.Name = "btnOpenTargetFolder";
            this.btnOpenTargetFolder.Size = new System.Drawing.Size(75, 23);
            this.btnOpenTargetFolder.TabIndex = 6;
            this.btnOpenTargetFolder.Text = "打开文件夹";
            this.btnOpenTargetFolder.UseVisualStyleBackColor = true;
            this.btnOpenTargetFolder.Click += new System.EventHandler(this.btnOpenTargetFolder_Click);
            // 
            // btnOpenTargetFile
            // 
            this.btnOpenTargetFile.AutoSize = true;
            this.btnOpenTargetFile.Location = new System.Drawing.Point(382, 116);
            this.btnOpenTargetFile.Name = "btnOpenTargetFile";
            this.btnOpenTargetFile.Size = new System.Drawing.Size(87, 23);
            this.btnOpenTargetFile.TabIndex = 5;
            this.btnOpenTargetFile.Text = "打开目标文件";
            this.btnOpenTargetFile.UseVisualStyleBackColor = true;
            this.btnOpenTargetFile.Click += new System.EventHandler(this.btnOpenTargetFile_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "文件路径：";
            // 
            // btnSelectTargetFile
            // 
            this.btnSelectTargetFile.AutoSize = true;
            this.btnSelectTargetFile.Location = new System.Drawing.Point(6, 29);
            this.btnSelectTargetFile.Name = "btnSelectTargetFile";
            this.btnSelectTargetFile.Size = new System.Drawing.Size(87, 23);
            this.btnSelectTargetFile.TabIndex = 0;
            this.btnSelectTargetFile.Text = "选择目标文件";
            this.btnSelectTargetFile.UseVisualStyleBackColor = true;
            this.btnSelectTargetFile.Click += new System.EventHandler(this.btnSelectTargetFile_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(117, 434);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(227, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "*导入字典前，系统将自动对目标文件备份";
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(12, 429);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "打开备份文件夹";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnDownFile
            // 
            this.btnDownFile.AutoSize = true;
            this.btnDownFile.Location = new System.Drawing.Point(107, 29);
            this.btnDownFile.Name = "btnDownFile";
            this.btnDownFile.Size = new System.Drawing.Size(111, 23);
            this.btnDownFile.TabIndex = 9;
            this.btnDownFile.Text = "从线上服务器下载";
            this.btnDownFile.UseVisualStyleBackColor = true;
            this.btnDownFile.Click += new System.EventHandler(this.btnDownFile_Click);
            // 
            // ImportSensitiveWordDictForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 464);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnImport);
            this.Controls.Add(this.groupBox1);
            this.MaximumSize = new System.Drawing.Size(596, 503);
            this.MinimumSize = new System.Drawing.Size(596, 503);
            this.Name = "ImportSensitiveWordDictForm";
            this.Text = "导入敏感词字典";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radiobtnStr;
        private System.Windows.Forms.RadioButton rbtnEnter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectSourceFile;
        private System.Windows.Forms.Button btnOpenSourceFolder;
        private System.Windows.Forms.Button btnOpenSourceFile;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnOpenTargetFolder;
        private System.Windows.Forms.Button btnOpenTargetFile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSelectTargetFile;
        private System.Windows.Forms.TextBox txtSourceFilePath;
        private System.Windows.Forms.TextBox txtPathOfTargetFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnUploadDict;
        private System.Windows.Forms.Button btnDownFile;
    }
}