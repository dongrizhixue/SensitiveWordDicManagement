﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWDM.Manage
{
    public static class TextFileManage
    {
        /// <summary>
        /// 从文本文件种获取字符串集合，以行区分
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <returns>从文件中读取并形成集合</returns>
        public static List<string> GetListStringByFile(string filePath)
        {
            List<string> listString;

            try
            {
                if (File.Exists(filePath))
                {
                    listString = new List<string>();
                    StreamReader sr = new StreamReader(filePath, Encoding.Default);
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        listString.Add(line);
                    }
                    sr.Dispose();
                }
                else
                {
                    listString = null;
                }
            }
            catch (Exception ex)
            {
                listString = null;
                Common.SaveTextLogExtend.WriteException(ex);
            }
            return listString;
        }

        public static string DownLoadDictFile(out string msg)
        {
            string strResult;
            try
            {
                string strFtpServer = System.Configuration.ConfigurationManager.AppSettings["FtpServer"];
                string strFtpUser = System.Configuration.ConfigurationManager.AppSettings["FtpUser"];
                string strFtpPassword = System.Configuration.ConfigurationManager.AppSettings["FtpPassword"];
                string strFtpDictFileName = System.Configuration.ConfigurationManager.AppSettings["FtpDictFileName"];
                if (string.IsNullOrEmpty(strFtpServer) || string.IsNullOrEmpty(strFtpUser) || string.IsNullOrEmpty(strFtpPassword) || string.IsNullOrEmpty(strFtpDictFileName))
                {
                    strResult = "";
                    msg = "系统未配置敏感词字典下载路径";
                }
                else
                {
                    string path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + @"\DictBackup";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    string destPath = Path.Combine(path, Path.GetFileNameWithoutExtension(strFtpDictFileName) + DateTime.Now.ToString("yyyy年MM月dd日HH时mm分ss秒") + Path.GetExtension(strFtpDictFileName));
                    string strDownLoad;
                    if (Common.FtpWeb.DownLoadFile(strFtpServer, strFtpUser, strFtpPassword, strFtpDictFileName, destPath, out strDownLoad))
                    {
                        if (File.Exists(destPath))
                        {
                            strResult = destPath;
                            msg = "成功";
                        }
                        else
                        {
                            msg = "文件保存到本地失败";
                            strResult = "";
                        }
                    }
                    else
                    {
                        strResult = "";
                        msg = strDownLoad;
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "下载文件出现异常，请注意软件配置";
                Common.SaveTextLogExtend.WriteException(ex);
                strResult = "";
            }
            return strResult;
        }
    }
}
